val kores_version: String by project
val kotlin_version: String by project
val kotlinx_immutable_version: String by project
val kotlinx_serialization_version: String by project
val kotlinx_coroutine_version: String by project
val logback_version: String by project
val eventsys_version: String by project
val eventsys_coroutine_version: String by project
val config_version: String by project
val jwiutils_version: String by project
val jackson_version: String by project
val redin_version: String by project
val fastutil_version: String by project

plugins {
    application
    kotlin("jvm") version "1.9.0"
    id("org.jetbrains.kotlin.plugin.serialization") version "1.9.0"
    id("com.github.hierynomus.license") version "0.16.1"
    id("maven-publish")
}

group = "com.koresframework"
version = rootProject.sourceSets.main.get().resources.find { it.name == "com.koresframework.atevist.base.version" }!!.readText()

application {
    mainClass.set("com.github.koresframework.ApplicationKt")
}

repositories {
    mavenCentral()
    maven(url = "https://gitlab.com/api/v4/projects/30392813/packages/maven") {
        name = "JGang"
    }
}

dependencies {

    implementation("org.jetbrains.kotlin:kotlin-stdlib:$kotlin_version")
    implementation("org.jetbrains.kotlin:kotlin-reflect:$kotlin_version")
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8:$kotlin_version")
    implementation("org.jetbrains.kotlinx:kotlinx-collections-immutable:$kotlinx_immutable_version")
    implementation("org.jetbrains.kotlinx:kotlinx-serialization-core:$kotlinx_serialization_version")
    implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:$kotlinx_serialization_version")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:$kotlinx_coroutine_version")

    api("com.github.koresframework:eventsys:$eventsys_version")
    api("com.koresframework:eventsys-coroutine:$eventsys_coroutine_version")
    api("com.github.jonathanxd:jwiutils:$jwiutils_version")
    api("com.github.jonathanxd:jwiutils-kt:$jwiutils_version")
    api("com.github.jonathanxd:specializations:$jwiutils_version")
    api("com.github.jonathanxd:links:$jwiutils_version")
    api("com.github.jonathanxd:properties:$jwiutils_version")
    api("com.github.jonathanxd:json-lang-loader:$jwiutils_version")
    implementation("com.github.jonathanxd:redin:$redin_version")
    implementation("it.unimi.dsi:fastutil:$fastutil_version")

    implementation("com.fasterxml.jackson.core:jackson-databind:$jackson_version")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin:$jackson_version")
    implementation("com.fasterxml.jackson.datatype:jackson-datatype-jsr310:$jackson_version")

    api("com.koresframework:kores:$kores_version.base")
    implementation("com.koresframework:kores-bytecodewriter:$kores_version.bytecode")

    implementation("ch.qos.logback:logback-classic:$logback_version")
    testImplementation(kotlin("test"))
    testImplementation("io.kotest:kotest-runner-junit5:4.6.1")
    testImplementation("io.kotest:kotest-assertions-core:4.6.1")
    testImplementation("io.kotest:kotest-property:4.6.1")
    testImplementation("org.junit.jupiter:junit-jupiter-api:5.7.2")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:5.7.2")
}

tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile>() {
    kotlinOptions.jvmTarget = "19"
    kotlinOptions.freeCompilerArgs = listOf("-Xjvm-default=all")
}

tasks.withType<Test> {
    useJUnitPlatform()
}

tasks {
    withType<nl.javadude.gradle.plugins.license.License> {
        header = rootProject.file("LICENSE")
        strictCheck = true
    }
}

kotlin {
    sourceSets.all {
        languageSettings.apply {
            languageVersion = "1.6"
        }
    }
}

val sourcesJar = tasks.create<Jar>("sourcesJar") {
    dependsOn("classes")
    this.archiveClassifier.set("sources")
    from(sourceSets.main.get().allSource)
}

publishing {
    repositories {
        maven {
            url = uri("${layout.buildDirectory}/repo")
        }
        maven {
            name = "GitLab"
            url = uri("https://gitlab.com/api/v4/projects/30398215/packages/maven")
            credentials(HttpHeaderCredentials::class) {
                val ciToken = System.getenv("CI_JOB_TOKEN")
                if (ciToken != null && ciToken.isNotEmpty()) {
                    name = "Job-Token"
                    value = System.getenv("CI_JOB_TOKEN")
                } else {
                    name = "Private-Token"
                    value = project.findProperty("GITLAB_TOKEN")?.toString() ?: System.getenv("GITLAB_TOKEN")
                }
            }
            authentication {
                val header by registering(HttpHeaderAuthentication::class)
            }
        }
        maven {
            name = "GitLabJgang"
            url = uri("https://gitlab.com/api/v4/projects/30392813/packages/maven")
            credentials(HttpHeaderCredentials::class) {
                val ciToken = System.getenv("CI_JOB_TOKEN")
                if (ciToken != null && ciToken.isNotEmpty()) {
                    name = "Job-Token"
                    value = System.getenv("CI_JOB_TOKEN")
                } else {
                    name = "Private-Token"
                    value = project.findProperty("GITLAB_TOKEN")?.toString() ?: System.getenv("GITLAB_TOKEN")
                }
            }
            authentication {
                val header by registering(HttpHeaderAuthentication::class)
            }
        }
        maven {
            name = "Space"
            url = uri("https://maven.pkg.jetbrains.space/oblitersoftware/p/opensource/kores")
            credentials {
                val spaceUserName = project.findProperty("spaceUsername")?.toString() ?: System.getenv("SPACE_USERNAME")
                val spacePassword = project.findProperty("spacePassword")?.toString() ?: System.getenv("SPACE_PASSWORD")
                username = spaceUserName
                password = spacePassword
            }
        }
    }
    publications {
        register("maven", MavenPublication::class) {
            from(components["kotlin"])
            artifactId = "atevist-base"
            artifact(sourcesJar)
        }
    }
}