job("Build test and publish") {
    startOn {
        gitPush {
            enabled = true
            branchFilter {
                +Regex("version\\/(.*)")
            }
        }
    }

    gradlew("openjdk:16", "build", "test", "publishAllPublicationsToSpaceRepository") {
        env["SPACE_USERNAME"] = Secrets("space_username")
        env["SPACE_PASSWORD"] = Secrets("space_password")
    }
}