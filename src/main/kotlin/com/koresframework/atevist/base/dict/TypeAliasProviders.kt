/**
 *      Atevist - Event propagation framework.
 *
 *         The MIT License (MIT)
 *
 *      Copyright (c) 2021 JonathanxD (https://github.com/JonathanxD/) <jhrldev@gmail.com>
 *      Copyright (c) 2021 KoresFramework (https://github.com/koresframework/)
 *      Copyright (c) 2021 contributors
 *
 *      Permission is hereby granted, free of charge, to any person obtaining a copy
 *      of this software and associated documentation files (the "Software"), to deal
 *      in the Software without restriction, including without limitation the rights
 *      to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *      copies of the Software, and to permit persons to whom the Software is
 *      furnished to do so, subject to the following conditions:
 *
 *      The above copyright notice and this permission notice shall be included in
 *      all copies or substantial portions of the Software.
 *
 *      THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *      IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *      FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *      AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *      THE SOFTWARE.
 */
package com.koresframework.atevist.base.dict

import com.koresframework.atevist.base.data.AtevistEvent
import com.koresframework.atevist.base.data.EventDispatchContainer
import com.koresframework.atevist.base.data.SerializableCharacteristic
import com.koresframework.atevist.base.data.SerializableProperty
import com.koresframework.eventsys.event.characteristic.CharacteristicKey
import com.koresframework.eventsys.event.characteristic.CharacteristicValue
import com.koresframework.eventsys.event.property.Property
import java.util.concurrent.CopyOnWriteArrayList

interface TypeAliasProviders : TypeAliasProvider {
    fun registerProvider(provider: TypeAliasProvider)
}

class TypeAliasProvidersImpl : TypeAliasProviders {
    private val providers = CopyOnWriteArrayList<TypeAliasProvider>()

    override fun registerProvider(provider: TypeAliasProvider) {
        if (!this.providers.contains(provider))
            this.providers.add(provider)
    }

    override fun getTypeForAlias(alias: String): Class<*>? =
        this.providers
            .firstNotNullOfOrNull { it.getTypeForAlias(alias) }

    override fun getAliasForClass(type: Class<*>): String? =
        this.providers
            .firstNotNullOfOrNull { it.getAliasForClass(type) }

    override fun getTypeForEvent(event: AtevistEvent, typeAlias: String): Class<*>? =
        this.providers
            .firstNotNullOfOrNull { it.getTypeForEvent(event, typeAlias) }

    override fun getTypeForProperty(property: SerializableProperty): Class<*>? =
        this.providers
            .firstNotNullOfOrNull { it.getTypeForProperty(property) }

    override fun getTypeForCharacteristicKey(characteristic: SerializableCharacteristic): Class<*>? =
        this.providers
            .firstNotNullOfOrNull { it.getTypeForCharacteristicKey(characteristic) }

    override fun getTypeForCharacteristicValue(characteristic: SerializableCharacteristic): Class<*>? =
        this.providers
            .firstNotNullOfOrNull { it.getTypeForCharacteristicValue(characteristic) }

    override fun getAliasForCharacteristicKey(characteristic: CharacteristicKey<*>): String? =
        this.providers
            .firstNotNullOfOrNull { it.getAliasForCharacteristicKey(characteristic) }

    override fun getAliasForCharacteristicValue(characteristic: CharacteristicValue<*, *>): String? =
        this.providers
            .firstNotNullOfOrNull { it.getAliasForCharacteristicValue(characteristic) }

    override fun getAliasForEvent(container: EventDispatchContainer<*>, type: Class<*>): String? =
        this.providers
            .firstNotNullOfOrNull { it.getAliasForEvent(container, type) }

    override fun getAliasForProperty(property: Property<*>): String? =
        this.providers
            .firstNotNullOfOrNull { it.getAliasForProperty(property) }

    override fun getTypeForExtensionClass(extensionClassAlias: String): Class<*>? =
        this.providers
            .firstNotNullOfOrNull { it.getTypeForExtensionClass(extensionClassAlias) }

    override fun getTypeForExtensionInterface(extensionInterfaceAlias: String): Class<*>? =
        this.providers
            .firstNotNullOfOrNull { it.getTypeForExtensionInterface(extensionInterfaceAlias) }

    override fun getAliasForExtensionClass(extensionClass: Class<*>): String? =
        this.providers
            .firstNotNullOfOrNull { it.getAliasForExtensionClass(extensionClass) }

    override fun getAliasForExtensionInterface(extensionInterface: Class<*>): String? =
        this.providers
            .firstNotNullOfOrNull { it.getAliasForExtensionInterface(extensionInterface) }
}