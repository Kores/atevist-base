/**
 *      Atevist - Event propagation framework.
 *
 *         The MIT License (MIT)
 *
 *      Copyright (c) 2021 JonathanxD (https://github.com/JonathanxD/) <jhrldev@gmail.com>
 *      Copyright (c) 2021 KoresFramework (https://github.com/koresframework/)
 *      Copyright (c) 2021 contributors
 *
 *      Permission is hereby granted, free of charge, to any person obtaining a copy
 *      of this software and associated documentation files (the "Software"), to deal
 *      in the Software without restriction, including without limitation the rights
 *      to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *      copies of the Software, and to permit persons to whom the Software is
 *      furnished to do so, subject to the following conditions:
 *
 *      The above copyright notice and this permission notice shall be included in
 *      all copies or substantial portions of the Software.
 *
 *      THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *      IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *      FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *      AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *      THE SOFTWARE.
 */
package com.koresframework.atevist.base.dict

import com.koresframework.atevist.base.data.AtevistEvent
import com.koresframework.atevist.base.data.EventDispatchContainer
import com.koresframework.atevist.base.data.SerializableCharacteristic
import com.koresframework.atevist.base.data.SerializableProperty
import com.koresframework.atevist.base.marker.Deserialization
import com.koresframework.atevist.base.marker.Serialization
import com.koresframework.eventsys.event.characteristic.CharacteristicKey
import com.koresframework.eventsys.event.characteristic.CharacteristicValue
import com.koresframework.eventsys.event.property.Property

/**
 * Provides [**type alias**][String] to [Class] and [Class] to [**type alias**][String] resolution.
 */
interface TypeAliasProvider {

    /**
     * Resolves the [Class] for a given [alias].
     */
    @Deserialization
    fun getTypeForAlias(alias: String): Class<*>?

    /**
     * Resolves the correct [Class] for a given [typeAlias] when
     * deserializing [event] type.
     *
     * The [typeAlias] may not be the event type itself, if the event does have generic information,
     * this method will be used to resolve these types as well.
     */
    @Deserialization
    fun getTypeForEvent(event: AtevistEvent, typeAlias: String) =
        this.getTypeForAlias(typeAlias)

    /**
     * Resolves the [Class] of a given [serialized property][property].
     */
    @Deserialization
    fun getTypeForProperty(property: SerializableProperty) =
        this.getTypeForAlias(property.value.type)

    /**
     * Resolves the [Class] of a given [serialized characteristic key][characteristic].
     */
    @Deserialization
    fun getTypeForCharacteristicKey(characteristic: SerializableCharacteristic) =
        this.getTypeForAlias(characteristic.type)

    /**
     * Resolves the [Class] of a given [serialized characteristic value][characteristic].
     */
    @Deserialization
    fun getTypeForCharacteristicValue(characteristic: SerializableCharacteristic) =
        this.getTypeForAlias(characteristic.value.type)

    /**
     * Resolves the [Class] of a given [extension class alias][extensionClassAlias].
     */
    @Deserialization
    fun getTypeForExtensionClass(extensionClassAlias: String) =
        this.getTypeForAlias(extensionClassAlias)

    /**
     * Resolves the [Class] of a given [extension interface alias][extensionInterfaceAlias].
     */
    @Deserialization
    fun getTypeForExtensionInterface(extensionInterfaceAlias: String) =
        this.getTypeForAlias(extensionInterfaceAlias)

    ///////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Resolves the alias of a given [type][type].
     */
    @Serialization
    fun getAliasForClass(type: Class<*>): String?

    /**
     * Resolves the alias of a given [characteristic key][characteristic].
     */
    @Serialization
    fun getAliasForCharacteristicKey(characteristic: CharacteristicKey<*>) =
        this.getAliasForClass(characteristic::class.java)

    /**
     * Resolves the alias of a given [characteristic value][characteristic].
     */
    @Serialization
    fun getAliasForCharacteristicValue(characteristic: CharacteristicValue<*, *>) =
        this.getAliasForClass(characteristic::class.java)

    /**
     * Resolves the correct [alias][String] for a given [type] when
     * serializing [event][container] type.
     *
     * The [type] may not be the event type itself, if the event does have generic information,
     * this method will be used to resolve these types as well.
     */
    @Serialization
    fun getAliasForEvent(container: EventDispatchContainer<*>, type: Class<*>) =
        this.getAliasForClass(type)

    /**
     * Resolves the alias of a given [property].
     */
    @Serialization
    fun getAliasForProperty(property: Property<*>) =
        this.getAliasForClass(property.type)

    /**
     * Resolves the alias of a given [extensionClass].
     */
    @Serialization
    fun getAliasForExtensionClass(extensionClass: Class<*>) =
        this.getAliasForClass(extensionClass)

    /**
     * Resolves the alias of a given [extensionInterface].
     */
    @Serialization
    fun getAliasForExtensionInterface(extensionInterface: Class<*>) =
        this.getAliasForClass(extensionInterface)

}