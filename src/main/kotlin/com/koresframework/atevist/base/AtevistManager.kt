/**
 *      Atevist - Event propagation framework.
 *
 *         The MIT License (MIT)
 *
 *      Copyright (c) 2021 JonathanxD (https://github.com/JonathanxD/) <jhrldev@gmail.com>
 *      Copyright (c) 2021 KoresFramework (https://github.com/koresframework/)
 *      Copyright (c) 2021 contributors
 *
 *      Permission is hereby granted, free of charge, to any person obtaining a copy
 *      of this software and associated documentation files (the "Software"), to deal
 *      in the Software without restriction, including without limitation the rights
 *      to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *      copies of the Software, and to permit persons to whom the Software is
 *      furnished to do so, subject to the following conditions:
 *
 *      The above copyright notice and this permission notice shall be included in
 *      all copies or substantial portions of the Software.
 *
 *      THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *      IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *      FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *      AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *      THE SOFTWARE.
 */
package com.koresframework.atevist.base

import com.koresframework.atevist.base.dict.TypeAliasProvider
import com.koresframework.atevist.base.event.AtevistBaseEvent
import com.koresframework.atevist.base.event.isInReplyTo
import com.koresframework.atevist.base.events.EventFactory
import com.koresframework.atevist.base.ser.CommonEventSerializer
import com.koresframework.atevist.base.ser.EventSerializer
import com.koresframework.eventsys.context.CLASS_LOADER
import com.koresframework.eventsys.context.EnvironmentContext
import com.koresframework.eventsys.coroutine.EventsChannelFactory
import com.koresframework.eventsys.coroutine.impl.EventsChannelFactoryImpl
import com.koresframework.eventsys.error.ExceptionListenError
import com.koresframework.eventsys.event.Event
import com.koresframework.eventsys.event.EventDispatcher
import com.koresframework.eventsys.event.EventListenerRegistry
import com.koresframework.eventsys.event.ListenerRegistryResults
import com.koresframework.eventsys.gen.event.CommonEventGenerator
import com.koresframework.eventsys.gen.event.EventGenerator
import com.koresframework.eventsys.impl.CommonEventDispatcher
import com.koresframework.eventsys.impl.DefaultEventManager
import com.koresframework.eventsys.impl.PerChannelEventListenerRegistry
import com.koresframework.eventsys.logging.Level
import com.koresframework.eventsys.logging.LoggerInterface
import com.koresframework.eventsys.logging.MessageType
import com.koresframework.eventsys.result.ListenResult
import com.koresframework.eventsys.util.createFactory
import com.koresframework.eventsys.util.getEventType
import com.koresframework.kores.bytecode.classloader.CodeClassLoader
import com.koresframework.kores.type.typeOf
import kotlinx.coroutines.channels.ReceiveChannel
import kotlinx.coroutines.flow.consumeAsFlow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.onStart
import org.slf4j.Logger
import java.lang.reflect.Type
import kotlin.coroutines.CoroutineContext

/**
 * Atevist Event Manager
 *
 * This interface provides an abstraction over **EventSys** managers, with common implementations
 * suitable to most of the workloads.
 *
 * ## Type Aliases
 *
 * Every event has a unique identification which is the event class name,
 * this identification is used to serialize and deserialize the
 * event.
 *
 * The problem with the identification, is that the event may be in a different package in the target, for example,
 * the **service A** may declare the event in `com.foo.bar.servicea.MessageEvent`, and the **service B** may declare
 * the same event in `com.foo.bar.serviceb.MessageEvent`.
 *
 * This problem does not happen if both services shares the same codebase for events, but if it does not,
 * type aliases must be configured.
 *
 * Type alises are not only for the event, if the event has **Type Parameters**, there must be an alias
 * for the types that does not share the same package definition.
 *
 * ## Remote Dispatch
 *
 * **Atevist Client** wraps the [localEventDispatcher] into a delegator that also dispatch events
 * to **Atevist Servers**.
 *
 * Given that, the correct way of dispatching events to **servers** is by using [AtevistManager.dispatch]
 * functions or the dispatch functions provided by the [eventManager].
 *
 * The [localEventDispatcher] only dispatch events locally, in the same application domain, events dispatched using
 * it will not be sent to **Atevist Servers**.
 */
interface AtevistManager {
    val eventGenerator: EventGenerator
    val eventListenerRegistry: EventListenerRegistry
    val localEventDispatcher: EventDispatcher
    val serializer: EventSerializer<AtevistBaseEvent>
    val eventManager: AtevistEventManager
    val eventFactory: EventFactory
    val eventsChannelFactory: EventsChannelFactory

    /**
     * Register an alias for a type.
     *
     * This provides a bidirectional resolution from [alias] to [type] and from [type] to [alias].
     */
    fun registerTypeAlias(alias: String, type: Class<*>)

    /**
     * Register a type alias provider.
     *
     * It is like [registerTypeAlias], but more generic and allow for context-based
     * resolution with more complex logics.
     */
    fun registerTypeAliasProvider(provider: TypeAliasProvider)

    /**
     * Registers all functions annotated with `@Listener` as an event listener.
     *
     * @param owner Identification of the class instance that is registering listeners.
     * @param listenerInstance Instance of the class which contains functions annotated with `@Listener`
     * @param loader Custom class loader to use to resolve [listenerInstance] class. This is important
     * in cases that the class loader of [listenerInstance class][listenerInstance] differs from the loader
     * that created the [EventGenerator].
     */
    fun registerListeners(owner: Any, listenerInstance: Any, loader: ClassLoader? = null): ListenerRegistryResults

    /**
     * Dispatches the provided [event] to listeners.
     *
     * @param event The event to sent
     * @param type The event type, including generic information. This value is also stored in the event itself,
     * defined when the event is created.
     * @param dispatcher The class instance that is dispatching the event.
     * @param ctx Context information.
     *
     * @see AtevistManager
     */
    suspend fun <T : AtevistBaseEvent> dispatch(
        event: T,
        type: Type = getEventType(event),
        dispatcher: Any,
        channel: String,
        ctx: EnvironmentContext = EnvironmentContext()
    ) =
        this.eventManager.dispatch(event, type, dispatcher, channel, ctx)

    /**
     * Creates a channel that receives events of provided [eventType].
     */
    fun <T: Event> on(eventType: Type): ReceiveChannel<T>
}

/**
 * Creates a channel that receives events of provided [event type][T].
 */
inline fun <reified T: Event> AtevistManager.on(): ReceiveChannel<T> =
    this.on(typeOf<T>())

/**
 * @see dispatchAndWaitReplyWith
 */
suspend inline fun <T : AtevistBaseEvent, reified R: AtevistBaseEvent> AtevistManager.dispatchAndWaitReply(
    event: T,
    type: Type = getEventType(event),
    dispatcher: Any,
    channel: String,
    ctx: EnvironmentContext = EnvironmentContext()
): R = this.dispatchAndWaitReplyWith(
    event = event,
    type = type,
    dispatcher = dispatcher,
    channel = channel,
    ctx = ctx,
    isReply = { it.isInReplyTo(event) }
)

/**
 * Register an Event Listener that waits for a reply and dispatch the event.
 *
 * This function guarantees that the event is only dispatched when the listener is already registered for the reply,
 * so it is not possible to the reply come before the listener is registered.
 *
 * Also, this function correctly propagate failure when the event does not reach **Atevist**, but it does not handle
 * timeout, for timeout use **Kotlin Coroutines facilities**, instead, the coroutine will be suspended forever.
 * Timeout should always be used to guarantee that the coroutine is not suspended forever.
 *
 * ## Do not use request-reply unless really necessary
 *
 * Atevist does not recommend request-reply pattern to be heavily used in Event oriented applications,
 * instead, prefer other approaches. However, there are scenarios that request-reply is appropriated, for example,
 * for the ping-pong logic.
 *
 * Request-reply is not recommended because it may be hard to identify slow processes, for example:
 *
 * ```
 * A send a request to B and waits for response
 * B send a request to C and waits for response
 * C send a request to D and waits for response
 * ```
 *
 * `A` will only receive a response when `D` completes its execution, which slow down the entire process, and blocks
 * resources that can be used for other things.
 *
 * > Since Atevist uses Kotlin Coroutines, blocking execution does not block the Thread, which allows
 * > Threads to do other important work, but it does not solve the *disconnect* problem.
 *
 * Instead, prefer to Pub-Sub and design your interface (UI/Apps) to work that way, so your interface (UI/Apps) reacts to events
 * instead of to replies. This is better for long-running tasks, you don't care about *losing the connection* with the other
 * end because of timeouts or page reloads.
 *
 * @param event The event to sent
 * @param type The event type, including generic information. This value is also stored in the event itself,
 * defined when the event is created.
 * @param dispatcher The class instance that is dispatching the event.
 * @param ctx Context information.
 * @param isReply Validates if the *received event* is a reply to [event].
 * @param T Event to dispatch
 * @param R The event type that is replied.
 *
 * @see AtevistManager
 */
suspend inline fun <T : AtevistBaseEvent, reified R: AtevistBaseEvent> AtevistManager.dispatchAndWaitReplyWith(
    event: T,
    type: Type = getEventType(event),
    dispatcher: Any,
    channel: String,
    ctx: EnvironmentContext = EnvironmentContext(),
    crossinline isReply: suspend (R) -> Boolean
): R =
    this.on<R>()
        .consumeAsFlow()
        .onStart {
            val result = dispatch(event, type = type, dispatcher = dispatcher, channel = channel, ctx = ctx)
            val listenResult = result.await().firstOrNull { it.eventListenerContainer.eventListener is AtevistFakeListener }?.result
            if (listenResult is ListenResult.Failed) {
                val error = listenResult.error
                if (error is ExceptionListenError) {
                    throw error.exception
                } else {
                    throw IllegalStateException(error.toString())
                }
            }
        }
        .first { isReply(it) }

class AtevistLogger(
    val logger: Logger
) : LoggerInterface {
    override fun log(message: String, messageType: MessageType, ctx: EnvironmentContext) {
        if (messageType.level == Level.FATAL) {
            throw IllegalStateException("Fatal error occurred: $message")
        } else {
            message.logAtLevel(messageType)
        }
    }

    override fun log(message: String, messageType: MessageType, throwable: Throwable, ctx: EnvironmentContext) {
        (message to throwable).logAtLevel(messageType)

        if (messageType.level == Level.FATAL) {
            throw throwable
        }
    }

    override fun log(messages: List<String>, messageType: MessageType, ctx: EnvironmentContext) {
        log(messages.joinToString("\n"), messageType, ctx)
    }

    override fun log(messages: List<String>, messageType: MessageType, throwable: Throwable, ctx: EnvironmentContext) {
        log(messages.joinToString("\n"), messageType, throwable, ctx)
    }

    private fun String.logAtLevel(type: MessageType) =
        with(type.level) { log("<${type.name}> ${this@logAtLevel}") }

    private fun Pair<String, Throwable>.logAtLevel(type: MessageType) =
        with(type.level) { log("<${type.name}> ${this@logAtLevel.first}", this@logAtLevel.second) }

    private fun Level.log(message: String) {
        when (this) {
            Level.TRACE -> this@AtevistLogger.logger.trace(message)
            Level.DEBUG -> this@AtevistLogger.logger.debug(message)
            Level.INFO -> this@AtevistLogger.logger.info(message)
            Level.WARN -> this@AtevistLogger.logger.warn(message)
            Level.FATAL, Level.ERROR -> this@AtevistLogger.logger.error(message)
        }
    }

    private fun Level.log(message: String, error: Throwable) {
        when (this) {
            Level.TRACE -> this@AtevistLogger.logger.trace(message, error)
            Level.DEBUG -> this@AtevistLogger.logger.debug(message, error)
            Level.INFO -> this@AtevistLogger.logger.info(message, error)
            Level.WARN -> this@AtevistLogger.logger.warn(message, error)
            Level.FATAL, Level.ERROR -> this@AtevistLogger.logger.error(message, error)
        }
    }

}

class AtevistManagerImpl(
    val atevist: Atevist,
    val logger: Logger,
    val context: CoroutineContext
) : AtevistManager {
    private val atevistLogger = AtevistLogger(this.logger)

    override val eventGenerator: EventGenerator = CommonEventGenerator(this.atevistLogger)
    override val eventListenerRegistry: EventListenerRegistry = PerChannelEventListenerRegistry(
        DefaultEventManager.COMMON_SORTER,
        this.atevistLogger,
        this.eventGenerator
    )

    override val localEventDispatcher: EventDispatcher = CommonEventDispatcher(
        DefaultEventManager.COMMON_THREAD_FACTORY,
        this.eventGenerator,
        this.atevistLogger,
        this.context,
        this.eventListenerRegistry
    )

    override val serializer: CommonEventSerializer = CommonEventSerializer(atevist.id, this.eventGenerator)
    override val eventManager: AtevistEventManager = AtevistEventManager(
        this.eventGenerator,
        this.localEventDispatcher,
        this.eventListenerRegistry,
        this.atevist,
        this.context,
        this.serializer
    )

    override val eventFactory: EventFactory = this.eventGenerator.createFactory<EventFactory>().resolve()

    override val eventsChannelFactory: EventsChannelFactory = EventsChannelFactoryImpl(this.eventListenerRegistry)

    override fun registerTypeAlias(alias: String, type: Class<*>) {
        this.serializer.dict.register(alias, type)
    }

    override fun registerTypeAliasProvider(provider: TypeAliasProvider) {
        this.serializer.providers.registerProvider(provider)
    }

    override fun registerListeners(owner: Any, listenerInstance: Any, loader: ClassLoader?): ListenerRegistryResults {
        val ctx = EnvironmentContext()

        if (loader != null) {
            CLASS_LOADER.set(ctx.data, CodeClassLoader(loader))
        }

        return this.eventManager.eventListenerRegistry.registerListeners(
            owner,
            listenerInstance,
            ctx
        )
    }

    override fun <T : Event> on(eventType: Type): ReceiveChannel<T> = this.eventsChannelFactory.channel(eventType)
}

fun Atevist.createManager(context: CoroutineContext = EVENT_LM): AtevistManager {
    return AtevistManagerImpl(
        this,
        this.logger,
        context
    )
}