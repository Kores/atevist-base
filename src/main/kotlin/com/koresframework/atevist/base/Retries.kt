/**
 *      Atevist - Event propagation framework.
 *
 *         The MIT License (MIT)
 *
 *      Copyright (c) 2021 JonathanxD (https://github.com/JonathanxD/) <jhrldev@gmail.com>
 *      Copyright (c) 2021 KoresFramework (https://github.com/koresframework/)
 *      Copyright (c) 2021 contributors
 *
 *      Permission is hereby granted, free of charge, to any person obtaining a copy
 *      of this software and associated documentation files (the "Software"), to deal
 *      in the Software without restriction, including without limitation the rights
 *      to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *      copies of the Software, and to permit persons to whom the Software is
 *      furnished to do so, subject to the following conditions:
 *
 *      The above copyright notice and this permission notice shall be included in
 *      all copies or substantial portions of the Software.
 *
 *      THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *      IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *      FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *      AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *      THE SOFTWARE.
 */
package com.koresframework.atevist.base

import kotlinx.coroutines.delay
import java.time.Duration

sealed class Retries {
    object Disabled : Retries()
    data class Times(val total: Int, val interval: Duration): Retries()
    data class Infinite(val interval: Duration): Retries()
}

suspend inline fun <reified R> Retries.doRetry(
    crossinline catch: suspend (Throwable, retry: Boolean) -> Unit,
    crossinline f: suspend () -> R
): Result<R> {
    return when (this) {
        is Retries.Disabled -> try {
            Result.success(f())
        } catch (t: Throwable) {
            catch(t, false)
            return Result.failure(t)
        }
        is Retries.Infinite -> {
            var lastError: Throwable? = null
            while (true) {
                try {
                    return Result.success(f())
                } catch (t: Throwable) {
                    lastError = t
                    catch(t, true)
                }
                delay(this.interval.toMillis())
            }
            return Result.failure(lastError!!)
        }
        is Retries.Times -> {
            var lastError: Throwable? = null
            val retries = 0 until this.total
            val retriesIter = retries.iterator()

            while (retriesIter.hasNext()) {
                retriesIter.nextInt()
                try {
                    return Result.success(f())
                } catch (t: Throwable) {
                    lastError = t
                    catch(t, retriesIter.hasNext())
                }
                delay(this.interval.toMillis())
            }
            return Result.failure(lastError!!)
        }
    }
}