/**
 *      Atevist - Event propagation framework.
 *
 *         The MIT License (MIT)
 *
 *      Copyright (c) 2021 JonathanxD (https://github.com/JonathanxD/) <jhrldev@gmail.com>
 *      Copyright (c) 2021 KoresFramework (https://github.com/koresframework/)
 *      Copyright (c) 2021 contributors
 *
 *      Permission is hereby granted, free of charge, to any person obtaining a copy
 *      of this software and associated documentation files (the "Software"), to deal
 *      in the Software without restriction, including without limitation the rights
 *      to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *      copies of the Software, and to permit persons to whom the Software is
 *      furnished to do so, subject to the following conditions:
 *
 *      The above copyright notice and this permission notice shall be included in
 *      all copies or substantial portions of the Software.
 *
 *      THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *      IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *      FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *      AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *      THE SOFTWARE.
 */
package com.koresframework.atevist.base.ser

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.module.SimpleModule
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import com.koresframework.atevist.base.data.*
import com.koresframework.atevist.base.dict.TypeAliasProviders
import com.koresframework.atevist.base.dict.TypeAliasProvidersImpl
import com.koresframework.atevist.base.dict.TypeDict
import com.koresframework.atevist.base.event.AtevistBaseEvent
import com.koresframework.atevist.base.event.RemoteEvent
import com.koresframework.atevist.base.event.ReplyingEvent
import com.koresframework.atevist.base.event.characteristic.Lifetime
import com.koresframework.atevist.base.event.characteristic.LifetimeValue
import com.koresframework.atevist.base.event.characteristic.Reply
import com.koresframework.atevist.base.event.characteristic.Replying
import com.koresframework.eventsys.event.Event
import com.koresframework.eventsys.event.characteristic.CharacteristicExt
import com.koresframework.eventsys.event.characteristic.CharacteristicHolder
import com.koresframework.eventsys.event.characteristic.CharacteristicKey
import com.koresframework.eventsys.event.characteristic.Characteristics
import com.koresframework.eventsys.event.property.GetterProperty
import com.koresframework.eventsys.extension.ExtensionOrder
import com.koresframework.eventsys.extension.ExtensionSpecification
import com.koresframework.eventsys.gen.event.EventGenerator
import com.koresframework.eventsys.util.create
import com.koresframework.kores.type.*
import com.koresframework.kores.util.genericTypeFromSourceString
import kotlinx.collections.immutable.persistentMapOf
import org.slf4j.LoggerFactory
import java.lang.reflect.Type
import java.time.Duration
import java.time.ZonedDateTime
import java.util.*

interface EventSerializer<T: AtevistBaseEvent> {
    val providers: TypeAliasProviders
    val dict: TypeDict

    fun serialize(eventContainer: EventDispatchContainer<T>): AtevistEvent
    fun deserialize(event: AtevistEvent): T
}

class CommonEventSerializer(
    val clientId: UUID,
    val generator: EventGenerator,
    val om: ObjectMapper = ObjectMapper().registerKotlinModule().findAndRegisterModules(),
    override val providers: TypeAliasProviders = TypeAliasProvidersImpl()
) : EventSerializer<AtevistBaseEvent> {

    private val module = SimpleModule().also {
        it.addSerializer(AtevistBaseEvent::class.java, WrappedSerializer(this))
        it.addDeserializer(AtevistBaseEvent::class.java, WrappedDeserializer(this))
    }

    private val logger = LoggerFactory.getLogger(CommonEventSerializer::class.java)
    override val dict: TypeDict = TypeDict()

    init {
        this.providers.registerProvider(this.dict)
        om.registerModule(this.module)
        om.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }

    private val loader: ClassLoader
        get() = generator.generationEnvironment.classLoader as? ClassLoader? ?: CommonEventSerializer::class.java.classLoader

    fun ClassLoader.resolve(type: String) =
        when (type) {
            "char" -> java.lang.Character.TYPE
            "byte" -> java.lang.Byte.TYPE
            "short" -> java.lang.Short.TYPE
            "int" -> java.lang.Integer.TYPE
            "void" -> java.lang.Void.TYPE
            "long" -> java.lang.Long.TYPE
            "double" -> java.lang.Double.TYPE
            "float" -> java.lang.Float.TYPE
            "boolean" -> java.lang.Boolean.TYPE
            else -> this.loadClass(type)
        }


    override fun serialize(eventContainer: EventDispatchContainer<AtevistBaseEvent>): AtevistEvent {
        val event = eventContainer.event

        val chars = if (event is CharacteristicHolder<*>) {
            event.characteristics.allCharacteristics.map { (k, v) ->
                val keyTypeName = this.providers.getAliasForCharacteristicKey(k) ?: k::class.java.binaryName
                val valueTypeName = this.providers.getAliasForCharacteristicValue(v) ?: v::class.java.binaryName

                Characteristic(
                    type = keyTypeName,
                    json = om.writeValueAsString(k),
                    value = CharacteristicValue(
                        type = valueTypeName,
                        om.writeValueAsString(v)
                    )
                )
            }
        } else {
            emptyList()
        }

        val eventUniqueId = event.eventUniqueId

        fun createSenderClientProperty(): SerializableProperty {
            val uuidName = this.providers.getAliasForClass(UUID::class.java) ?: UUID::class.java.binaryName
            val json = om.writeValueAsString(clientId)
            return SerializableProperty(
                "senderClient", PropertyValue(uuidName, json)
            )
        }

        fun createEventUniqueIdProperty(): SerializableProperty {
            val uuidName = this.providers.getAliasForClass(UUID::class.java) ?: UUID::class.java.binaryName
            val json = om.writeValueAsString(eventUniqueId)
            return SerializableProperty(
                "eventUniqueId", PropertyValue(uuidName, json)
            )
        }

        val remoteEventProperties = listOf(
            createSenderClientProperty(),
            createEventUniqueIdProperty()
        )

        val typeName = event.eventType.eventBaseType().asGeneric.serial(eventContainer)
        val serialProperties = event.getProperties().mapNotNull {
            if (chars.isNotEmpty() && it.key == "characteristics")
                return@mapNotNull null

            val propTypeName = this.providers.getAliasForProperty(it.value) ?: it.value.type.binaryName
            val property = it.value
            if (property is GetterProperty<*>) {
                Property(it.key, PropertyValue(propTypeName, om.writeValueAsString(property.getValue())))
            } else {
                null
            }
        } + remoteEventProperties

        val interfaceExtensions = event.extensionInterfaces.map {
            this.providers.getAliasForExtensionInterface(it) ?: it.binaryName
        }

        val implementationExtensions = event.extensionClasses.map {
            this.providers.getAliasForExtensionClass(it) ?: it.binaryName
        }


        val replyEvent = ((event as? ReplyingEvent)?.inReplyTo
            ?: (event as? CharacteristicHolder<*>)?.let {
                (it.characteristics.get(Reply) as? Replying)?.event
            })

        val keep = (event as? CharacteristicHolder<*>)?.let {
            (it.characteristics.get(Lifetime) as? LifetimeValue)?.lifetime
        } ?: Duration.ZERO

        val inReplyTo = replyEvent?.let {
            if (it is RemoteEvent) {
                ReplyTarget(it.senderClient, it.eventUniqueId)
            } else {
                logger.error("Could not reply an event that is not remote.")
                null
            }
        }

        return AtevistEvent(
            uniqueId = eventUniqueId,
            origin = clientId,
            eventTypeId = typeName,
            inReplyTo = inReplyTo,
            createdAt = ZonedDateTime.now(),
            keep = keep,
            channels = eventContainer.channelSet.toSet(),
            properties = serialProperties,
            implementationExtensions = implementationExtensions,
            interfaceExtensions = interfaceExtensions,
            characteristics = chars
        )
    }

    override fun deserialize(event: AtevistEvent): AtevistBaseEvent {
        val type = event.eventTypeId.deserial(event)
        val inReplyTo = event.inReplyTo

        val characteristics = event.characteristics.map {
            val keyType = this.providers.getTypeForCharacteristicKey(it) ?: loader.resolve(it.type)
            val valueType = this.providers.getTypeForCharacteristicValue(it) ?: loader.resolve(it.value.type)
            val key = om.readValue(it.json, keyType) as CharacteristicKey<*>
            val value = om.readValue(it.value.json, valueType) as com.koresframework.eventsys.event.characteristic.CharacteristicValue<*, *>
            key to value
        }

        val interfaceExtensions = event.interfaceExtensions.map {
            this.providers.getTypeForExtensionInterface(it) ?: loader.resolve(it)
        }.filter {
            !it.`is`(typeOf<RemoteEvent>()) && !it.`is`(typeOf<CharacteristicHolder<*>>())
        }.map {
            ExtensionSpecification(
                residence = this,
                implement = it,
                extensionClass = null,
                ExtensionOrder()
            )
        }

        val classExtensions = event.implementationExtensions.map {
            this.providers.getTypeForExtensionClass(it) ?: loader.resolve(it)
        }.filter {
            !it.`is`(typeOf<CharacteristicExt<*>>())
        }.map {
            ExtensionSpecification(
                residence = this,
                implement = null,
                extensionClass = it,
                ExtensionOrder()
            )
        }

        val extensions = interfaceExtensions + classExtensions + listOf(
            ExtensionSpecification(residence = this, implement = typeOf<RemoteEvent>(), extensionClass = null, ExtensionOrder())
        )

        val allExtension = if (characteristics.isNotEmpty()) {
            extensions + listOf(
                ExtensionSpecification(
                    residence = this,
                    implement = typeOf<CharacteristicHolder<*>>(),
                    extensionClass = typeOf<CharacteristicExt<*>>(),
                    ExtensionOrder()
                )
            )
        } else {
            extensions
        }

        val eventClass = generator.createEventClass<Event>(
            type = type,
            additionalProperties = emptyList(),
            extensions = allExtension
        )

        val resolved = eventClass.resolve()

        val properties = event.properties.associate {
            val propType = this.providers.getTypeForProperty(it) ?: loader.resolve(it.value.type)
            if (it.name == "characteristics") {
                it.name to Characteristics.Persistent()
            } else {
                it.name to om.readValue(it.value.json, propType)
            }
        }

        val allProperties = if (characteristics.isNotEmpty()) {
            properties + mapOf("characteristics" to Characteristics.Persistent(
                persistentMapOf(*characteristics.toTypedArray())
            ))
        } else {
            properties
        }
        // TODO: Ensure create(?, ?) returns AtevistBaseEvent
        return create(resolved, allProperties) as AtevistBaseEvent
    }

    fun Type.eventBaseType(): Type {
        if (this is GenericType) {
            if (this.name == "E") {
                return this.bounds.first().type
            }
        }

        return this
    }

    fun String.deserial(event: AtevistEvent): GenericType {
        return genericTypeFromSourceString(this) {
            (providers.getTypeForEvent(event, it) ?: loader.resolve(it)).koresType
        }
    }

    fun GenericType.serial(container: EventDispatchContainer<*>): String {
        val sb = StringBuilder()

        if (this.isType) {
            sb.append(this.canonicalName(container))
        } else {
            if (!this.isWildcard) {
                sb.append(this.name)
            } else {
                sb.append("?")
            }
        }


        val bounds = this.bounds

        if (bounds.isNotEmpty()) {

            for (i in bounds.indices) {

                val hasNext = i + 1 < bounds.size

                val bound = bounds[i]

                val extendsOrSuper = bound.sign == "+" || bound.sign == "-"

                if (bound.sign == "+") {
                    sb.append(" extends ")
                } else if (bound.sign == "-") {
                    sb.append(" super ")
                } else {
                    if (i == 0) {
                        sb.append("<")
                    }
                }

                val type = bound.type

                if (type is GenericType) {
                    sb.append(type.serial(container))
                } else {
                    sb.append(type.canonicalName(container))
                }

                if (!extendsOrSuper && !hasNext) {
                    sb.append(">")
                }

                if (hasNext && extendsOrSuper) {
                    sb.append(" &")
                } else if (hasNext) {
                    sb.append(", ")
                }
            }

        }

        return sb.toString()
    }

    private fun Type.canonicalName(container: EventDispatchContainer<*>): String {
        val resolved = this.bindedDefaultResolver.resolve().rightOrNull() as Class<*>
        return providers.getAliasForEvent(container, resolved) ?: this.canonicalName
    }

}