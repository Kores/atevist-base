/**
 *      Atevist - Event propagation framework.
 *
 *         The MIT License (MIT)
 *
 *      Copyright (c) 2021 JonathanxD (https://github.com/JonathanxD/) <jhrldev@gmail.com>
 *      Copyright (c) 2021 KoresFramework (https://github.com/koresframework/)
 *      Copyright (c) 2021 contributors
 *
 *      Permission is hereby granted, free of charge, to any person obtaining a copy
 *      of this software and associated documentation files (the "Software"), to deal
 *      in the Software without restriction, including without limitation the rights
 *      to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *      copies of the Software, and to permit persons to whom the Software is
 *      furnished to do so, subject to the following conditions:
 *
 *      The above copyright notice and this permission notice shall be included in
 *      all copies or substantial portions of the Software.
 *
 *      THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *      IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *      FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *      AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *      THE SOFTWARE.
 */
package com.koresframework.atevist.base

import com.koresframework.atevist.base.data.AtevistEvent
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*
import org.slf4j.LoggerFactory
import java.time.Duration
import java.util.*

/**
 * An abstraction over [flows][Flow] for event dispatching.
 *
 * Implementations should support event retrial and store the events waiting for retrial on [retryFlow]. Also,
 * everytime an event is processed it must be sent to [dispatchedEventsFlow], which broadcasts the attempt
 * to dispatch the event, regardless the attempt succeeded or failed.
 *
 * Read [AbstractEventListenFlow] for more.
 */
interface EventListenFlow {
    val listenFlow: SharedFlow<DispatchEventData>
    val retryFlow: SharedFlow<RetryQueueElement>
    val dispatchedEventsFlow: SharedFlow<DispatchedEvent>

    suspend fun emit(dispatch: DispatchEventData)
    suspend fun collectRetrying(id: UUID, f: EventListenFunction)
}

/**
 * Common implementation of [EventListenFlow] that properly supports retrial for redundant and non-redundant events.
 *
 * ## Implementation
 *
 * The implementation consist of merging [listenFlow] and [retryFlow] into a single flow of data. Once an event is
 * taken, it tries to call the provided [EventListenFunction] to handle the event, if the call fails with an exception
 * or returns a [Result Failure][Result.failure], it checks if retry is possible (within the [retries] configuration),
 * if it is, is uses the provided [scope] to launch a **Coroutine** to emit the event to [retryFlow]. If it is not possible,
 * it completes the [DispatchEventData] with the failure and the event is sent to [dispatchedEventsFlow]. It is important
 * to be aware that the event is always sent to [dispatchedEventsFlow], regardless if it failed to be dispatched or not.
 *
 * This mechanism also allows that concurrent coroutines that is blocked on [collectRetrying] can take an event that
 * failed to be handled on a different coroutine. Ensuring that different coroutines can try to consume the value,
 * and the first one to succeed will complete the [DispatchEventData] with [Unit] (which represents success). However,
 * for events with [redundancy][DispatchEventData.isRedundant], all consumers receives the event, so an additional
 * property, called [forId][RetryQueueElement.forId], is set to store which coroutine is allowed to consume
 * the event (since all of them will be consuming and putting on retrial, this avoids event duplication).
 *
 * For events that does not have redundancy and must only be processed by one coroutine, the [DispatchEventData.taken]
 * is used to signal other coroutines that the event was already taken to be processed, if it fails to be processed,
 * a new [DispatchEventData] is created with a different instance of [DispatchEventData.taken] configured, which
 * allows a new coroutine to take the event and try to handle it.
 *
 * This logic guarantees that retrial happens not only for redundant events, but to non-redundant events as well; and
 * allows different coroutines to take the event and try, instead of retrying always on the same coroutine. But to be
 * guaranteed that the same coroutine does not take the same event when the coroutines fail repeatedly (or is disconnected),
 * the coroutine must suspend before trying to receive the next element of the flow.
 */
abstract class AbstractEventListenFlow(
    val retries: Retries,
    val scope: CoroutineScope,
): EventListenFlow {
    private val logger = LoggerFactory.getLogger(AbstractEventListenFlow::class.java)
    override val listenFlow: MutableSharedFlow<DispatchEventData> = MutableSharedFlow()
    override val retryFlow: MutableSharedFlow<RetryQueueElement> = MutableSharedFlow()
    private val dispatchedEvents_ = MutableSharedFlow<DispatchedEvent>()
    override val dispatchedEventsFlow: SharedFlow<DispatchedEvent>
        get() = this.dispatchedEvents_

    override suspend fun emit(dispatch: DispatchEventData) {
        this.listenFlow.emit(dispatch)
    }

    // Implementation note:
    // There is two types of retrial: Per-client retrial and Per-event retrial
    // Per-client retrial must always happen, if the client received the event, it must retry if
    // it fails to send the event
    // Per-event retrial is client-agnostic, it does not matter if it failed on Client A, we should
    // retry on any clients that are available, however, we must not exceed the amount of configured retries,
    // even if happens on different clients
    // To implement this we use an additional SharedFlow that keeps track of events to retry
    // and create a Channel that receives results of both RetryFlow and ListenFlow
    // Also, to ensure that only one client handle an event when the event does not use redundancy
    // we call `tryTake` and only handle the event if it returns `true`.
    override suspend fun collectRetrying(id: UUID, f: EventListenFunction) {
        flow {
            val retryFlow = retryFlow.filter {
                it.forId == null || it.forId == id
            }
            val listenFlow = listenFlow
                .map {
                    RetryQueueElement(
                        forId = if(it.isRedundant) id else null,
                        container =  it.createRetryContainer().updateTry()
                    )
                }

            emitAll(listOf(retryFlow, listenFlow).merge())
        }.filter {
            it.tryTake()
        }.collect {
            val result = try {
                f(it.container.data.event)
            } catch (t: Throwable) {
                Result.failure(RuntimeException("Uncaught exception occurred inside collectRetrying(address) {...} function.", t))
            }

            if (result.isSuccess) {
                it.container.data.complete.complete(Unit)
                this.dispatchedEvents_.emit(DispatchedEvent(
                    it.container.data.event,
                    it.container.data.complete,
                    id,
                    retrying = false,
                    succeeded = true,
                    exception = null
                ))
            } else {
                val e = result.exceptionOrNull()!!
                val fail = it.updateFailed()

                if (fail != null) {
                    logger.error("Failed to send event to Atevist, retrying...", e)
                    this.dispatchedEvents_.emit(DispatchedEvent(
                        it.container.data.event,
                        it.container.data.complete,
                        id,
                        succeeded = false,
                        retrying = true,
                        exception = e
                    ))
                    scope.launch {
                        retryFlow.emit(fail)
                    }
                } else {
                    logger.error("Failed to send event to Atevist, not retrying.", e)
                    it.container.data.complete.completeExceptionally(e)
                    this.dispatchedEvents_.emit(DispatchedEvent(
                        it.container.data.event,
                        it.container.data.complete,
                        id,
                        succeeded = false,
                        retrying = false,
                        exception = e
                    ))
                }
            }
        }
    }

    private fun DispatchEventData.createRetryContainer(): RetryContainer =
        retries.createRetryContainer(this)
}

/**
 * @see AbstractEventListenFlow
 */
class EventListenFlowImpl(retries: Retries, scope: CoroutineScope) : AbstractEventListenFlow(retries, scope)

data class DispatchedEvent(
    val event: AtevistEvent,
    val complete: CompletableDeferred<Unit>,
    val id: UUID,
    val succeeded: Boolean,
    val retrying: Boolean,
    val exception: Throwable?
)

data class RetryQueueElement(
    val forId: UUID?,
    val container: RetryContainer
) {
    fun updateFailed() =
        container.updateFailed()?.let { container -> this.copy(container = container) }

    fun tryTake() = this.container.data.isRedundant || this.container.data.taken.complete(Unit)
}

/**
 * @property currentTries Amount of times that was already tried.
 */
data class RetryContainer(
    val data: DispatchEventData,
    val currentTries: Int,
    val maxRetries: Int,
    val retrialInterval: Duration
) {
    fun updateSuccess(): RetryContainer? = null
    fun updateFailed(): RetryContainer? =
        if (data.isRedundant) updateFailedWithAllow()
        else updateFailedWithoutAllow()

    private fun updateFailedWithoutAllow(): RetryContainer? =
        if (maxRetries == Int.MAX_VALUE || currentTries + 1 <= maxRetries) {
            this.copy(
                currentTries = currentTries + 1
            )
        } else {
            null
        }

    private fun updateFailedWithAllow(): RetryContainer? =
        if (maxRetries == Int.MAX_VALUE || currentTries + 1 <= maxRetries) {
            this.copy(
                data = data.copy(taken = CompletableDeferred()),
                currentTries = currentTries + 1
            )
        } else {
            null
        }

    fun canRetry() = currentTries + 1 <= maxRetries

    fun updateTry() = this.copy(currentTries = 1)
}

fun Retries.createRetryContainer(data: DispatchEventData): RetryContainer =
    if (this !is Retries.Disabled) {
        val maxRetries = (this as? Retries.Times)?.total ?: Integer.MAX_VALUE
        val interval = (this as? Retries.Times)?.interval
            ?: (this as? Retries.Infinite)?.interval
            ?: Duration.ZERO

        RetryContainer(
            data,
            0,
            maxRetries = maxRetries,
            retrialInterval = interval
        )
    } else RetryContainer(
        data,
        0,
        maxRetries = 1,
        retrialInterval = Duration.ZERO
    )
