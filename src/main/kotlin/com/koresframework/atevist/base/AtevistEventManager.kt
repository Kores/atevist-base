/**
 *      Atevist - Event propagation framework.
 *
 *         The MIT License (MIT)
 *
 *      Copyright (c) 2021 JonathanxD (https://github.com/JonathanxD/) <jhrldev@gmail.com>
 *      Copyright (c) 2021 KoresFramework (https://github.com/koresframework/)
 *      Copyright (c) 2021 contributors
 *
 *      Permission is hereby granted, free of charge, to any person obtaining a copy
 *      of this software and associated documentation files (the "Software"), to deal
 *      in the Software without restriction, including without limitation the rights
 *      to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *      copies of the Software, and to permit persons to whom the Software is
 *      furnished to do so, subject to the following conditions:
 *
 *      The above copyright notice and this permission notice shall be included in
 *      all copies or substantial portions of the Software.
 *
 *      THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *      IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *      FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *      AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *      THE SOFTWARE.
 */
package com.koresframework.atevist.base

import com.koresframework.atevist.base.data.AtevistEvent
import com.koresframework.atevist.base.data.EventDispatchContainer
import com.koresframework.atevist.base.event.AtevistBaseEvent
import com.koresframework.atevist.base.event.characteristic.Lifetime
import com.koresframework.atevist.base.event.characteristic.LifetimeValue
import com.koresframework.atevist.base.event.characteristic.Redundancy
import com.koresframework.atevist.base.event.characteristic.RedundancyValue
import com.koresframework.atevist.base.ser.CommonEventSerializer
import com.koresframework.atevist.base.ser.EventSerializer
import com.koresframework.eventsys.channel.ChannelSet
import com.koresframework.eventsys.context.EnvironmentContext
import com.koresframework.eventsys.error.ExceptionListenError
import com.koresframework.eventsys.event.Event
import com.koresframework.eventsys.event.EventDispatcher
import com.koresframework.eventsys.event.EventListener
import com.koresframework.eventsys.event.EventListenerRegistry
import com.koresframework.eventsys.event.characteristic.CharacteristicHolder
import com.koresframework.eventsys.gen.event.EventGenerator
import com.koresframework.eventsys.impl.CommonEventManager
import com.koresframework.eventsys.impl.EventListenerContainer
import com.koresframework.eventsys.result.DispatchResult
import com.koresframework.eventsys.result.ListenExecutionResult
import com.koresframework.eventsys.result.ListenResult
import kotlinx.coroutines.CompletableDeferred
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.lang.reflect.Type
import java.time.Duration
import java.time.ZonedDateTime
import java.util.*
import java.util.concurrent.ConcurrentSkipListSet
import java.util.concurrent.ThreadLocalRandom
import kotlin.coroutines.CoroutineContext

class AtevistEventManager(
    eventGenerator: EventGenerator,
    eventDispatcher: EventDispatcher,
    eventListenerRegistry: EventListenerRegistry,
    val atevist: Atevist,
    val context: CoroutineContext,
    val serial: EventSerializer<AtevistBaseEvent> = CommonEventSerializer(atevist.id, eventGenerator)
): CommonEventManager(
    eventGenerator,
    EventDispatcherWrapper(atevist, serial, eventDispatcher, context),
    eventListenerRegistry
) {
    private val knownEventsSet = ConcurrentSkipListSet<UUID>()
    private val random = ThreadLocalRandom.current()

    /**
     * Provides a view of all known events which have not expired yet.
     *
     * For consistency reasons, events without an expiration may appear briefly in this set,
     * but they are always removed after the check step passes.
     *
     * All events that have redundancy configured appears in this set, this guarantees
     * that the same event is not handled more than one time when more than one server
     * sends the event to the client.
     */
    val knownEvents get() = knownEventsSet.toSet()

    init {
        CoroutineScope(this.context).launch {
            while (!atevist.dispatchChannel.isClosedForReceive) {
                val receive = atevist.dispatchChannel.receiveCatching()
                if (receive.isSuccess) {
                    val atevistEvent = receive.getOrThrow()
                    // Check before creating the event to avoid unnecessary creation overhead.
                    if (!onReceive(atevistEvent)) {
                        atevist.logger.debug("Dropped duplicated event: `$atevistEvent`.")
                    } else {
                        val event = serial.deserialize(atevistEvent)
                        eventDispatcher.dispatch(
                            event,
                            event.eventType,
                            this@AtevistEventManager,
                            ChannelSet.Include(atevistEvent.channels).joinToString(),
                            true,
                            EnvironmentContext()
                        )
                    }
                } else if (receive.isClosed) {
                    break
                }
            }
        }
    }

    fun onReceive(event: AtevistEvent): Boolean {
        if (!this.knownEventsSet.add(event.uniqueId)) return false

        val now = ZonedDateTime.now()
        val eventCreation = event.createdAt
        if (!event.keep.isZero) {
            val life = event.keep
            return if (Duration.between(eventCreation, now) > life) {
                this.knownEventsSet.remove(event.uniqueId)
                false
            } else {
                val expireAfter = life.plusMillis(random.nextLong(1, 1500))
                // Coroutines are very lightweight, so this may not introduce so much overhead,
                // or at least it may not introduce as much overhead as using a timer to clean-up
                // expired entries.
                // The random expiration delay is to try to avoid expiring all keys at once
                // We might reconsider this in the future.
                CoroutineScope(EXPIRATION_DISPATCHER).launch {
                    delay(expireAfter.toMillis())
                    knownEventsSet.remove(event.uniqueId)
                }
                true
            }
        } else {
            this.knownEventsSet.remove(event.uniqueId)
        }

        return true
    }
}

class AtevistFakeListener(val result: ListenResult) : EventListener<Event> {
    override suspend fun onEvent(event: Event, dispatcher: Any): ListenResult {
        return this.result
    }

}

class EventDispatcherWrapper(
    val atevist: Atevist,
    val serial: EventSerializer<AtevistBaseEvent>,
    val wrappedDispatcher: EventDispatcher,
    val context: CoroutineContext,
): EventDispatcher {
    override suspend fun <T : Event> dispatch(
        event: T,
        eventType: Type,
        dispatcher: Any,
        channel: String,
        isAsync: Boolean,
        ctx: EnvironmentContext
    ): DispatchResult<T> {
        val hasRedundancy = ((event as? CharacteristicHolder<*>)?.characteristics?.get(Redundancy) as? RedundancyValue)?.enabled == true
        val lifetime = ((event as? CharacteristicHolder<*>)?.characteristics?.get(Lifetime) as? LifetimeValue)?.lifetime

        val transformedEvent = if (hasRedundancy && lifetime == null) {
            (event as CharacteristicHolder<*>)
                .characteristic(Lifetime, LifetimeValue(this.atevist.retention)) as T
        } else {
            event
        }

        val ev = serial.serialize(EventDispatchContainer(
            ChannelSet.Expression.parseExpression(channel),
            transformedEvent as AtevistBaseEvent
        ))

        val complete = CompletableDeferred<ListenExecutionResult<T>>()

        val dispatchComplete = CompletableDeferred<Unit>()
        val data = DispatchEventData(
            ev,
            hasRedundancy,
            CompletableDeferred(),
            dispatchComplete
        )

        this.atevist.listenFlow.emit(data)
        val sendResult = try {
            Result.success(dispatchComplete.await())
        } catch (e: Exception) {
            Result.failure(e)
        }

        val listenResult = if (sendResult.isSuccess) {
            ListenResult.Value(sendResult.getOrThrow())
        } else {
            ListenResult.Failed(
                ExceptionListenError(sendResult.exceptionOrNull()!!)
            )
        }

        val dummyListener = AtevistFakeListener(listenResult)
        complete.complete(
            ListenExecutionResult(
                EventListenerContainer(this, eventType, dummyListener),
                transformedEvent,
                eventType,
                dispatcher,
                channel,
                listenResult,
                ctx
            )
        )

        val result = DispatchResult(this.context, listOf(complete))

        return result.combine(this.wrappedDispatcher.dispatch(transformedEvent, eventType, dispatcher, channel, isAsync, ctx))
    }


}