/**
 *      Atevist - Event propagation framework.
 *
 *         The MIT License (MIT)
 *
 *      Copyright (c) 2021 JonathanxD (https://github.com/JonathanxD/) <jhrldev@gmail.com>
 *      Copyright (c) 2021 KoresFramework (https://github.com/koresframework/)
 *      Copyright (c) 2021 contributors
 *
 *      Permission is hereby granted, free of charge, to any person obtaining a copy
 *      of this software and associated documentation files (the "Software"), to deal
 *      in the Software without restriction, including without limitation the rights
 *      to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *      copies of the Software, and to permit persons to whom the Software is
 *      furnished to do so, subject to the following conditions:
 *
 *      The above copyright notice and this permission notice shall be included in
 *      all copies or substantial portions of the Software.
 *
 *      THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *      IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *      FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *      AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *      THE SOFTWARE.
 */
package com.koresframework.atevist.base.event

import com.koresframework.atevist.base.event.characteristic.Reply
import com.koresframework.atevist.base.event.characteristic.Replying
import com.koresframework.eventsys.event.Event
import com.koresframework.eventsys.event.annotation.Extension
import com.koresframework.eventsys.event.characteristic.CharacteristicHolder
import java.time.ZonedDateTime
import java.util.*

@Extension(extensionClass = AtevistEventExt::class)
interface AtevistBaseEvent : Event {
    val eventUniqueId: UUID
    val eventCreationDate: ZonedDateTime
}

fun AtevistBaseEvent.isInReplyTo(other: AtevistBaseEvent) =
    (this as? CharacteristicHolder<*>)?.characteristics?.get(Reply)?.let {
        ((it as? Replying)?.event as? AtevistBaseEvent)?.eventUniqueId == other.eventUniqueId
    } ?: (this as? ReplyingEvent)?.inReplyTo?.let {
        (it as? AtevistBaseEvent)?.eventUniqueId == other.eventUniqueId
    } ?: false