/**
 *      Atevist - Event propagation framework.
 *
 *         The MIT License (MIT)
 *
 *      Copyright (c) 2021 JonathanxD (https://github.com/JonathanxD/) <jhrldev@gmail.com>
 *      Copyright (c) 2021 KoresFramework (https://github.com/koresframework/)
 *      Copyright (c) 2021 contributors
 *
 *      Permission is hereby granted, free of charge, to any person obtaining a copy
 *      of this software and associated documentation files (the "Software"), to deal
 *      in the Software without restriction, including without limitation the rights
 *      to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *      copies of the Software, and to permit persons to whom the Software is
 *      furnished to do so, subject to the following conditions:
 *
 *      The above copyright notice and this permission notice shall be included in
 *      all copies or substantial portions of the Software.
 *
 *      THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *      IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *      FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *      AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *      THE SOFTWARE.
 */
package com.koresframework.atevist.base.reply

import com.koresframework.atevist.base.data.EventDispatchContainer
import com.koresframework.atevist.base.event.RemoteEvent
import com.koresframework.atevist.base.event.characteristic.Reply
import com.koresframework.atevist.base.event.characteristic.Replying
import com.koresframework.atevist.base.event.isInReplyTo
import com.koresframework.atevist.base.events.PingEvent
import com.koresframework.atevist.base.ser.CommonEventSerializer
import com.koresframework.eventsys.channel.ChannelSet
import com.koresframework.eventsys.event.Event
import com.koresframework.eventsys.event.annotation.Extension
import com.koresframework.eventsys.gen.event.CommonEventGenerator
import com.koresframework.eventsys.impl.CommonLogger
import com.koresframework.eventsys.util.createFactory
import io.kotest.core.spec.style.FunSpec
import io.kotest.matchers.shouldBe
import io.kotest.matchers.types.shouldBeInstanceOf
import java.util.*

class SerialReply : FunSpec({
    test("serial") {
        val generator = CommonEventGenerator(CommonLogger())
        val factory = generator.createFactory<Factory>().resolve()
        val event = factory.createPingEvent()
        val ser = CommonEventSerializer(UUID.randomUUID(), generator)
        val serialized = ser.serialize(EventDispatchContainer(ChannelSet.ALL, event))
        val deserial = ser.deserialize(serialized)

        println(serialized)
        println(deserial)
        println((deserial as RemoteEvent).eventUniqueId)
        println((deserial as RemoteEvent).senderClient)

        val nEvent = event.characteristic(Reply, Replying(event))

        val nSerialized = ser.serialize(EventDispatchContainer(ChannelSet.ALL, nEvent))
        val nDeserial = ser.deserialize(nSerialized)

        println(nSerialized)
        println(nDeserial)
        println((nDeserial as RemoteEvent).eventUniqueId)
        println((nDeserial as RemoteEvent).senderClient)
        println((nDeserial as PingEvent).characteristics.get(Reply))
        nEvent.isInReplyTo(event).shouldBe(true)
    }
})

interface Factory {
    fun createPingEvent(): PingEvent
}